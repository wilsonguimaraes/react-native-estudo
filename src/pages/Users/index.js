import React, {Component, useState, useEffect} from 'react';
import {
  View,
  Text,
  Button,
  FlatList,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import api from '../../services/api';

export default class Users extends Component {
  state = {
    docs: [],
    counter: 0,
  };

  componentDidMount() {
    this.loadProducts();
  }

  navigateToHome = (navigation) => {
    navigation.navigate('Home', {
      screen: 'Settings', //Abre o link na aba Settings
    });
  };

  loadProducts = async () => {
    const response = await api.get('/products');
    const {docs} = response.data;
    //console.log(docs);

    //this.setState({counter: docs.length});
    this.setState({docs});
  };

  renderItem = ({item}) => (
    <View style={styles.productContainer}>
      <Text style={styles.productTitle}>{item.title}</Text>
      <Text style={styles.productDescription}>{item.description}</Text>
      <TouchableOpacity
        style={styles.productButton}
        onPress={() => {
          this.props.navigation.navigate('Product', {product: item});
        }}>
        <Text style={styles.productButtonText}>Acessar</Text>
      </TouchableOpacity>
    </View>
  );

  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        {/* <Text>Home3</Text> */}

        <FlatList
          contentContainerStyle={styles.list}
          data={this.state.docs}
          keyExtractor={(item) => item._id}
          renderItem={this.renderItem}
        />

        <Button
          title="Navigate to home2"
          onPress={() => this.navigateToHome(navigation)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
  },

  list: {
    padding: 20,
  },

  productContainer: {
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 5,
    padding: 20,
    marginBottom: 20,
  },

  productTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#333',
  },

  productDescription: {
    fontSize: 16,
    color: '#999',
    marginTop: 5,
    lineHeight: 24,
  },

  productButton: {
    height: 42,
    borderRadius: 5,
    borderWidth: 2,
    borderColor: '#7159c1',
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },

  productButtonText: {
    fontSize: 16,
    color: '#7159c1',
    fontWeight: 'bold',
  },
});
