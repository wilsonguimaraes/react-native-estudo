import React from 'react';
import {Text} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

//import Home from './pages/Home';
import Users from './pages/Users';
import Product from './pages/Product';

import DashboardRoutes from './dashboard.routes';

export default function Routes() {
  return (
    <Stack.Navigator
      initialRouteName="Users"
      screenOptions={{
        headerStyle: {backgroundColor: '#7159c1'},
        headerTintColor: '#fff',
      }}>
      <Stack.Screen
        name="Home"
        component={DashboardRoutes}
        options={{title: ' Dashboard'}}
      />
      <Stack.Screen name="Users" component={Users} />
      <Stack.Screen
        name="Product"
        component={Product}
        //options={{title: ' Product2'}}
      />
    </Stack.Navigator>
  );
}
