import React, {useState, useEffect} from 'react';
import api from '../../services/api';
import {View, Text, Button} from 'react-native';

// import { Container } from './styles';

export default function Users({navigation}) {

  function navigateToHome() {
    navigation.navigate('Home', {
      screen: 'Settings', //Abre o link na aba Settings
    });
  }

  //useEffect(() => this.loadProducts());

  useEffect(() => loadProducts());

  const loadProducts = async () => {
    const response = await api.get('/products');
    const {docs} = response.data;
    console.log(docs);
  };

  return (
    <View>
      <Text>Users</Text>
      <Button title="Navigate to home3" onPress={navigateToHome} />
    </View>
  );
}
