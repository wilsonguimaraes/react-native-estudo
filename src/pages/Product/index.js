import React from 'react';
import {Text} from 'react-native';
import {WebView} from 'react-native-webview';

function Product({route, navigation}) {
  //console.log(navigation);
  const {product} = route.params;
  navigation.setOptions({title: product.title});
  //return <Text>{product.url}</Text>;
  return <WebView source={{uri: product.url}} />;
}

export default Product;
